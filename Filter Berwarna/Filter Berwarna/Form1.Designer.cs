﻿namespace Filter_Berwarna
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnload = new System.Windows.Forms.Button();
            this.btnnoise = new System.Windows.Forms.Button();
            this.btnfilter = new System.Windows.Forms.Button();
            this.btntepi = new System.Windows.Forms.Button();
            this.btnsharpness = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.SuspendLayout();
            // 
            // btnload
            // 
            this.btnload.Location = new System.Drawing.Point(35, 12);
            this.btnload.Name = "btnload";
            this.btnload.Size = new System.Drawing.Size(98, 41);
            this.btnload.TabIndex = 0;
            this.btnload.Text = "LOAD";
            this.btnload.UseVisualStyleBackColor = true;
            this.btnload.Click += new System.EventHandler(this.btnload_Click);
            // 
            // btnnoise
            // 
            this.btnnoise.Location = new System.Drawing.Point(150, 12);
            this.btnnoise.Name = "btnnoise";
            this.btnnoise.Size = new System.Drawing.Size(95, 41);
            this.btnnoise.TabIndex = 1;
            this.btnnoise.Text = "Noise Gaussian";
            this.btnnoise.UseVisualStyleBackColor = true;
            this.btnnoise.Click += new System.EventHandler(this.btnnoise_Click);
            // 
            // btnfilter
            // 
            this.btnfilter.Location = new System.Drawing.Point(260, 14);
            this.btnfilter.Name = "btnfilter";
            this.btnfilter.Size = new System.Drawing.Size(95, 39);
            this.btnfilter.TabIndex = 2;
            this.btnfilter.Text = "Filter Rata-rata";
            this.btnfilter.UseVisualStyleBackColor = true;
            this.btnfilter.Click += new System.EventHandler(this.btnfilter_Click);
            // 
            // btntepi
            // 
            this.btntepi.Location = new System.Drawing.Point(370, 14);
            this.btntepi.Name = "btntepi";
            this.btntepi.Size = new System.Drawing.Size(95, 39);
            this.btntepi.TabIndex = 3;
            this.btntepi.Text = "Deteksi tepi";
            this.btntepi.UseVisualStyleBackColor = true;
            this.btntepi.Click += new System.EventHandler(this.btntepi_Click);
            // 
            // btnsharpness
            // 
            this.btnsharpness.Location = new System.Drawing.Point(485, 14);
            this.btnsharpness.Name = "btnsharpness";
            this.btnsharpness.Size = new System.Drawing.Size(96, 39);
            this.btnsharpness.TabIndex = 4;
            this.btnsharpness.Text = "Sharpness";
            this.btnsharpness.UseVisualStyleBackColor = true;
            this.btnsharpness.Click += new System.EventHandler(this.btnsharpness_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Location = new System.Drawing.Point(35, 76);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(277, 224);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 5;
            this.pictureBox1.TabStop = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Location = new System.Drawing.Point(333, 76);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(291, 224);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 6;
            this.pictureBox2.TabStop = false;
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(635, 443);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.btnsharpness);
            this.Controls.Add(this.btntepi);
            this.Controls.Add(this.btnfilter);
            this.Controls.Add(this.btnnoise);
            this.Controls.Add(this.btnload);
            this.Name = "Form1";
            this.Text = "Modul 14 Filter Berwarna";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnload;
        private System.Windows.Forms.Button btnnoise;
        private System.Windows.Forms.Button btnfilter;
        private System.Windows.Forms.Button btntepi;
        private System.Windows.Forms.Button btnsharpness;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
    }
}

